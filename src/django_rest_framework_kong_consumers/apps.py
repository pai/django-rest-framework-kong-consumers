from django.apps import AppConfig


class DjangoRestFrameworkKongConsumersConfig(AppConfig):
    name = 'django_rest_framework_kong_consumers'
