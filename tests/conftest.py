#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Dummy conftest.py for django_rest_framework_kong_consumers.

    If you don't know what this is for, just leave it empty.
    Read more about conftest.py under:
    https://pytest.org/latest/plugins.html
"""

# import pytest
